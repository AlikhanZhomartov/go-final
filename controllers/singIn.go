package controllers

import (
	"database/sql"
	"fmt"
	"html/template"
	"log"
	"net/http"
)

type User struct {
	Id uint16
	Email, Password, Role string
}

func Login(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r,"session-name")
	if err != nil{
		panic(err)
	}
	role := session.Values["role"]

	if role == "student" {
		http.Redirect(w, r, "/user/main", http.StatusSeeOther)
	}else if role == "employee" {
		http.Redirect(w, r, "/employee/main", http.StatusSeeOther)
	}else {

		t, err := template.ParseFiles("ui/html/login.html")

		if err != nil {
			fmt.Fprintf(w, err.Error())
		}

		err = t.Execute(w, nil)
		if err != nil {
			fmt.Fprintf(w, err.Error())
		}
	}
}

func Registration(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, "session-name")
	if err != nil {
		panic(err)
	}
	role := session.Values["role"]
	if role == "student" {
		http.Redirect(w, r, "/user/main", http.StatusSeeOther)
	}else if role == "employee" {
		http.Redirect(w, r, "/employee/main", http.StatusSeeOther)
	}else {
		t, err := template.ParseFiles("ui/html/registration.html")

		if err != nil {
			fmt.Fprintf(w, err.Error())
		}

		err = t.Execute(w, nil)
		if err != nil {
			fmt.Fprintf(w, err.Error())
		}

	}
}

func Create(w http.ResponseWriter, r *http.Request) {

	email := r.FormValue("Email")
	password := r.FormValue("Password")
	person := r.FormValue("Gender")

	db, err := sql.Open("mysql", "root:2131@tcp(127.0.0.1:3306)/final_first")
	if err != nil {
		panic(err)
	}

	defer db.Close()

	if email == "" || password == "" || person == "" {
		http.Redirect(w, r, "/registration", http.StatusSeeOther)
	}else {
		insert, err := db.Query(fmt.Sprintf("Insert into `user` (`email`, `password`, `role`) values('%s', '%s', '%s')", email, password, person))
		if err != nil {
			panic(err)
		}

		defer insert.Close()
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}
}

func SuccessLogin(w http.ResponseWriter, r *http.Request) {

	email := r.FormValue("Email")
	password := r.FormValue("Password")

	db, err := sql.Open("mysql", "root:2131@tcp(127.0.0.1:3306)/final_first")
	if err != nil {
		panic(err)
	}

	defer db.Close()

	if email == "" || password == "" {
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}else {
		res, err := db.Query(fmt.Sprintf("Select * from `user` where `email` = '%s' and `password` = '%s'", email, password))
		if err != nil {
			panic(err)
		}

		user1 := User{}

		for res.Next() {
			var user User
			err = res.Scan(&user.Id, &user.Email, &user.Password, &user.Role)
			if err != nil {
				panic(err)
			}
			user1 = user
		}


		if user1.Role == "" {
			http.Redirect(w, r, "/", http.StatusSeeOther)
		} else if user1.Role == "student"{

			session, err := store.Get(r, "session-name")
			if err != nil {
				log.Fatal(err)
			}
			session.Values["id"] = user1.Id
			session.Values["role"] = user1.Role

			err = session.Save(r, w)
			if err != nil {
				log.Fatal(err)
			}

			http.Redirect(w, r, "/user/main", http.StatusSeeOther)

		} else if user1.Role == "employee" {
			session, _ := store.Get(r, "session-name")
			session.Values["id"] = user1.Id
			session.Values["role"] = user1.Role

			session.Save(r, w)

			http.Redirect(w, r, "/employee/main", http.StatusSeeOther)
		}

	}
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func Logout(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, "session-name")
	if err != nil{
		log.Fatal(err)
	}
	session.Values["id"] = nil
	session.Values["role"] = nil
	session.Options.MaxAge = 0
	session.Save(r, w)
	http.Redirect(w, r, "/", http.StatusSeeOther)
}