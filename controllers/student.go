package controllers

import (
	"Go-final-first/protobp/proto"
	"context"
	"database/sql"
	"fmt"
	"github.com/gorilla/sessions"
	"google.golang.org/grpc"
	"html/template"
	"io"
	"log"
	"net/http"
	"strconv"
)
var store = sessions.NewCookieStore([]byte("SESSION_KEY"))
type Vacancy struct {
	Id             uint32
	JobTitle       string
	CompanyTitle   string
	City           string
	Salary         int32
	JobDescription string
	UserId         uint32
}

type ResumeClass struct {
	Id uint
	UserName string
	UserSurname string
	Mobile string
	City string
	UserId uint
}

func MainStudent(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r,"session-name")
	if err != nil{
		panic(err)
	}
	role := session.Values["role"]

	if role == "student" {

		conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
		if err != nil {
			log.Fatalf("could not connect: %v", err)
		}
		defer conn.Close()

		c := proto.NewStudentMainServiceClient(conn)

		t, _ := template.ParseFiles("ui/html/mainStudent.html")


		err = t.Execute(w, studentMain(c, r, w))
		if err != nil {
			panic(err)
		}
	} else{
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}
}

func Resume(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r,"session-name")
	if err != nil{
		panic(err)
	}
	role := session.Values["role"]

	if role == "student" {
		db, err := sql.Open("mysql", "root:2131@tcp(127.0.0.1:3306)/final_first")
		if err != nil {
			panic(err)
		}

		defer db.Close()



		session, err := store.Get(r, "session-name")
		if err != nil {
			panic(err)
		}
		userId := session.Values["id"]

		res, err := db.Query(fmt.Sprintf("select * from `resume` where `user_id` = '%d'", userId))
		if err != nil {
			panic(err)
		}

		resumeSend := ResumeClass{}
		for res.Next() {
			var resume ResumeClass
			err = res.Scan(&resume.Id, &resume.UserName, &resume.UserSurname, &resume.Mobile, &resume.City, &resume.UserId)
			if err != nil {
				panic(err)
			}
			resumeSend = resume
		}
		t, err := template.ParseFiles("ui/html/resume.html")
		if err != nil {
			log.Fatal(err)
		}
		_ = t.Execute(w, resumeSend)
	}else{
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}
}

func ResumeSave(w http.ResponseWriter, r *http.Request) {

	Name := r.FormValue("name")
	Surname := r.FormValue("surname")
	Mobile := r.FormValue("mobile")
	City := r.FormValue("city")
	Id := r.FormValue("resume_btn")

	session, err := store.Get(r,"session-name")
	if err != nil{
		panic(err)
	}
	userId := session.Values["id"]



	db, err := sql.Open("mysql", "root:2131@tcp(127.0.0.1:3306)/final_first")
	if err != nil {
		panic(err)
	}

	defer db.Close()

	res, err := db.Query(fmt.Sprintf("select * from `resume` where `user_id` = '%d'", userId))
    if err != nil {
		log.Fatal(err)
	}

	var resume ResumeClass
	resume.UserName = "Error"
	for res.Next() {
		err = res.Scan(&resume.Id, &resume.UserName, &resume.UserSurname, &resume.Mobile, &resume.City, &resume.UserId)
		if err != nil {
			panic(err)
		}
	}
	if resume.UserName == "Error" {
		db.Query(fmt.Sprintf("insert into `resume`(user_name, user_surname, user_mobile, user_city, user_id) values('%s', '%s', '%s', '%s', '%d')", Name, Surname, Mobile, City, userId))
	}else{
		db.Query(fmt.Sprintf("update `resume` set `user_name` = '%s', `user_surname` = '%s', `user_mobile` = '%s', `user_city` = '%s' where `Id` = '%s'", Name, Surname, Mobile, City, Id))
	}

	http.Redirect(w, r, "/user/resume", http.StatusSeeOther)
}

func MakeResponse(w http.ResponseWriter, r *http.Request){

	vacancyId := r.FormValue("job_btn")
	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer conn.Close()

	c := proto.NewMakeResponseServiceClient(conn)

	db, err := sql.Open("mysql", "root:2131@tcp(127.0.0.1:3306)/final_first")
	if err != nil {
		panic(err)
	}

	defer db.Close()

	session, err := store.Get(r,"session-name")
	if err != nil{
		panic(err)
	}
	userId := session.Values["id"]
	var resumeId int


	 err = db.QueryRow(fmt.Sprintf("select `Id` from `resume` where `user_id` = '%d'", userId)).Scan(&resumeId)
	if err != nil {
		log.Fatal(err)
	}


	ResponseMake(c, vacancyId, uint32(resumeId))

	http.Redirect(w, r, "/user/main", http.StatusSeeOther)
}


func studentMain(c proto.StudentMainServiceClient, r *http.Request, w http.ResponseWriter) []Vacancy{

	db, err := sql.Open("mysql", "root:2131@tcp(127.0.0.1:3306)/final_first")
	if err != nil {
		panic(err)
	}

	defer db.Close()

	session, err := store.Get(r,"session-name")
	if err != nil{
		panic(err)
	}

	var resumeId string

	err = db.QueryRow(fmt.Sprintf("select `Id` from `resume` where `user_id` = '%d'", session.Values["id"])).Scan(&resumeId)

	if resumeId == "" {
		http.Redirect(w, r, "/user/resume", http.StatusSeeOther)
	}


	ctx := context.Background()


	req := &proto.StudentMainRequest{Message: resumeId}


	stream, err := c.StudentMain(ctx, req)
	if err != nil {
		log.Fatalf("error while calling GreetManyTimes RPC %v", err)
	}
	defer stream.CloseSend()

	var vacancyArray []Vacancy

LOOP:
	for {
		res, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				// we've reached the end of the stream
				break LOOP
			}
			log.Fatalf("error while reciving from GreetManyTimes RPC %v", err)
		}
		//log.Printf("response from GreetManyTimes:%v \n", res.GetVacancy().JobTitle)

		vacancy := Vacancy{res.GetVacancy().Id, res.GetVacancy().JobTitle,
			res.GetVacancy().CompanyTitle, res.GetVacancy().City,
			res.GetVacancy().Salary,
			res.GetVacancy().JobDescription,
			res.GetVacancy().EmployeeId,
		}
		vacancyArray = append(vacancyArray, vacancy)
	}

	return vacancyArray

}

func ResponseMake(c proto.MakeResponseServiceClient, vacancyId string, resumeId uint32) {
	ctx := context.Background()

		vacancy, _ := strconv.ParseUint(vacancyId, 10 ,64)

	request := &proto.Request{VacancyId: vacancy, ResumeId: resumeId}

	_, err := c.MakeResponse(ctx, request)
	if err != nil {
		log.Fatalf("error while calling Greet RPC %v", err)
	}
}