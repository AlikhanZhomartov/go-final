package controllers

import (
	"Go-final-first/protobp/proto"
	"context"
	"database/sql"
	"fmt"
	"google.golang.org/grpc"
	"html/template"
	"io"
	"log"
	"net/http"
)

type ResponsesListStruct struct {
	JobTitle string
	CompanyTitle string
    UserName string
	UserSurname string
	UserPhone string
	UserCity string
	ResponseId int32
}

func EmployeeMain(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r,"session-name")
	if err != nil{
		panic(err)
	}
	role := session.Values["role"]

	if role == "employee" {

		conn, err := grpc.Dial("localhost:50052", grpc.WithInsecure())
		if err != nil {
			log.Fatalf("could not connect: %v", err)
		}
		defer conn.Close()

		c := proto.NewEmployeeMainServiceClient(conn)

		t, _ := template.ParseFiles("ui/html/mainEmployee.html")


		err = t.Execute(w, MainEmployee(c))
		if err != nil {
			panic(err)
		}
	} else{
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}
}


func MainEmployee(c proto.EmployeeMainServiceClient) []Vacancy{


	ctx := context.Background()
	req := &proto.EmployeeMainRequest{Message: "employee"}


	stream, err := c.EmployeeMain(ctx, req)
	if err != nil {
		log.Fatalf("error while calling GreetManyTimes RPC %v", err)
	}
	defer stream.CloseSend()

	var vacancyArray []Vacancy

LOOP:
	for {
		res, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				// we've reached the end of the stream
				break LOOP
			}
			log.Fatalf("error while reciving from GreetManyTimes RPC %v", err)
		}
		//log.Printf("response from GreetManyTimes:%v \n", res.GetVacancy().JobTitle)

		vacancy := Vacancy{res.GetVacancy().Id, res.GetVacancy().JobTitle,
			res.GetVacancy().CompanyTitle, res.GetVacancy().City,
			res.GetVacancy().Salary,
			res.GetVacancy().JobDescription,
			res.GetVacancy().EmployeeId,
		}
		vacancyArray = append(vacancyArray, vacancy)
	}

	return vacancyArray

}

func CreateVacancy(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r,"session-name")
	if err != nil{
		panic(err)
	}
	role := session.Values["role"]

	if role == "employee" {

		t, err := template.ParseFiles("ui/html/createVacancy.html")

		if err != nil {
			fmt.Fprintf(w, err.Error())
		}

		err = t.Execute(w, nil)
		if err != nil {
			fmt.Fprintf(w, err.Error())
		}


	}else{
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}
}

func CreatedVacancy(w http.ResponseWriter, r *http.Request) {
	jobTitle := r.FormValue("job_title")
	companyTitle := r.FormValue("company_title")
	city := r.FormValue("jober_city")
	salary := r.FormValue("salary")
	description := r.FormValue("job_description")

	conn, err := grpc.Dial("localhost:50052", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer conn.Close()

	c := proto.NewCreateVacancyServiceClient(conn)

	VacancyCreated(c, r, jobTitle, companyTitle, city, salary, description)

	http.Redirect(w, r, "/employee/vacancy/create", http.StatusSeeOther)

}

func VacancyCreated(c proto.CreateVacancyServiceClient, r *http.Request, jobTitle string, companyTitle string, city string, salary string, description string){


	session, err := store.Get(r,"session-name")
	if err != nil{
		panic(err)
	}
	id := fmt.Sprintf("%d",session.Values["id"])


	ctx := context.Background()


	request := &proto.CreateRequest{JobTitle: jobTitle, CompanyTitle: companyTitle, City: city, Salary: salary, Description: description, EmployeeId: id}

	_, err = c.CreateVacancyService(ctx, request)
	if err != nil {
		log.Fatalf("error while calling Greet RPC %v", err)
	}
}

func OwnVacancy(w http.ResponseWriter, r *http.Request) {

	session, err := store.Get(r,"session-name")
	if err != nil{
		panic(err)
	}
	role := session.Values["role"]

	if role == "employee" {

		conn, err := grpc.Dial("localhost:50052", grpc.WithInsecure())
		if err != nil {
			log.Fatalf("could not connect: %v", err)
		}
		defer conn.Close()

		c := proto.NewOwnVacancyServiceClient(conn)

		t, _ := template.ParseFiles("ui/html/ownVacancy.html")


		err = t.Execute(w, VacancyOwn(c, r))
		if err != nil {
			panic(err)
		}
	} else{
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}

}

func VacancyOwn(c proto.OwnVacancyServiceClient, r *http.Request) []Vacancy{
	session, err := store.Get(r,"session-name")
	if err != nil{
		panic(err)
	}
	employeeId := session.Values["id"]
    id := fmt.Sprintf("%d", employeeId)

	ctx := context.Background()


	req := &proto.OwnRequest{EmployeeId: id}


	stream, err := c.OwnVacancy(ctx, req)
	if err != nil {
		log.Fatalf("error while calling GreetManyTimes RPC %v", err)
	}
	defer stream.CloseSend()

	var vacancyArray []Vacancy

LOOP:
	for {
		res, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				// we've reached the end of the stream
				break LOOP
			}
			log.Fatalf("error while reciving from GreetManyTimes RPC %v", err)
		}
		//log.Printf("response from GreetManyTimes:%v \n", res.GetVacancy().JobTitle)

		vacancy := Vacancy{res.GetVacancyId(), res.GetJobTitle(),
			res.GetCompanyTitle(), res.GetCity(),
			res.GetSalary(),
			res.GetDescription(),
			0,
		}
		vacancyArray = append(vacancyArray, vacancy)
	}

	return vacancyArray
}

func DeleteVacancy(w http.ResponseWriter, r *http.Request) {
	vacancyId := r.FormValue("Id")

	conn, err := grpc.Dial("localhost:50052", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer conn.Close()

	c := proto.NewDeleteVacancyServiceClient(conn)

	VacancyDelete(c, vacancyId)

	http.Redirect(w, r, "/employee/own/vacancy", http.StatusSeeOther)

}

func VacancyDelete(c proto.DeleteVacancyServiceClient, vacancyId string){
	ctx := context.Background()

	request := &proto.DeleteRequest{VacancyId: vacancyId}

	_, err := c.DeleteVacancy(ctx, request)
	if err != nil {
		log.Fatalf("error while calling Greet RPC %v", err)
	}
}

func ResponsesList(w http.ResponseWriter, r *http.Request) {

	session, err := store.Get(r,"session-name")
	if err != nil{
		panic(err)
	}
	role := session.Values["role"]

	if role == "employee" {

		conn, err := grpc.Dial("localhost:50052", grpc.WithInsecure())
		if err != nil {
			log.Fatalf("could not connect: %v", err)
		}
		defer conn.Close()

		c := proto.NewResponsesListServiceClient(conn)

		t, _ := template.ParseFiles("ui/html/responsesList.html")

		err = t.Execute(w, ListResponses(c, r))
		if err != nil {
			log.Fatal(err)
		}
	}else{
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}


}

func ListResponses(c proto.ResponsesListServiceClient, r *http.Request) []ResponsesListStruct{
	session, err := store.Get(r,"session-name")
	if err != nil{
		panic(err)
	}
	employeeId := session.Values["id"]
	id := fmt.Sprintf("%d", employeeId)

	ctx := context.Background()


	req := &proto.ResponsesRequest{EmployeeId: id}


	stream, err := c.ResponsesList(ctx, req)
	if err != nil {
		log.Fatalf("error while calling GreetManyTimes RPC %v", err)
	}
	defer stream.CloseSend()

	db, err := sql.Open("mysql", "root:2131@tcp(127.0.0.1:3306)/final_first")
	if err != nil {
		panic(err)
	}

	defer db.Close()

	var responsesArray []ResponsesListStruct
LOOP:
	for {
		res, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				break LOOP
			}
			log.Fatalf("error while reciving from GreetManyTimes RPC %v", err)
		}


		var Name    string
		var Surname string
		var Mobile  string
		var City    string

		err = db.QueryRow(fmt.Sprintf("select `user_name`, `user_surname`, `user_mobile`, `user_city` from `resume` where `Id` = '%d'", res.GetResumeId())).Scan(&Name, &Surname, &Mobile, &City)
		if err != nil {
			log.Fatal(err)
		}


		responsesList := ResponsesListStruct{res.GetJobTitle(), res.GetCompanyTitle(),
			Name, Surname,
			Mobile,
			City,
			res.ResponseId,
		}
		responsesArray = append(responsesArray, responsesList)
	}

	return responsesArray

}

func DeleteResponse(w http.ResponseWriter, r *http.Request) {
	responseId := r.FormValue("Id")

	conn, err := grpc.Dial("localhost:50052", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer conn.Close()

	c := proto.NewDeleteResponseServiceClient(conn)

	ResponseDelete(c, responseId)

	http.Redirect(w, r, "/employee/responses", http.StatusSeeOther)
}

func ResponseDelete(c proto.DeleteResponseServiceClient, responseId string) {

	ctx := context.Background()

	request := &proto.DeleteResponseRequest{ResponseId: responseId}

	_, err := c.DeleteResponse(ctx, request)
	if err != nil {
		log.Fatalf("error while calling Greet RPC %v", err)
	}

}