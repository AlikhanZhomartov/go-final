package main

import (
	"Go-final-first/controllers"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"net/http"
)

func handleFunc() {
	rtr := mux.NewRouter()

	rtr.HandleFunc("/", controllers.Login).Methods("GET")
	rtr.HandleFunc("/registration", controllers.Registration).Methods("GET")
	rtr.HandleFunc("/registration/create", controllers.Create).Methods("POST")
	rtr.HandleFunc("/login/success", controllers.SuccessLogin).Methods("POST")
	rtr.HandleFunc("/user/main", controllers.MainStudent).Methods("GET")
	rtr.HandleFunc("/user/resume", controllers.Resume).Methods("GET")
	rtr.HandleFunc("/user/resume/save", controllers.ResumeSave).Methods("POST")
	rtr.HandleFunc("/user/main/response", controllers.MakeResponse).Methods("POST")
	rtr.HandleFunc("/logout", controllers.Logout).Methods("GET")
	rtr.HandleFunc("/employee/main", controllers.EmployeeMain).Methods("GET")
	rtr.HandleFunc("/employee/vacancy/create", controllers.CreateVacancy).Methods("GET")
	rtr.HandleFunc("/employee/vacancy/created", controllers.CreatedVacancy).Methods("POST")
	rtr.HandleFunc("/employee/own/vacancy", controllers.OwnVacancy).Methods("GET")
	rtr.HandleFunc("/employee/vacancy/delete", controllers.DeleteVacancy).Methods("POST")
	rtr.HandleFunc("/employee/responses", controllers.ResponsesList).Methods("GET")
	rtr.HandleFunc("/employee/responses/delete", controllers.DeleteResponse).Methods("POST")

	http.Handle("/resources/", http.StripPrefix("/resources/", http.FileServer(http.Dir("./ui/static/css/"))))
	http.Handle("/img/", http.StripPrefix("/img/", http.FileServer(http.Dir("./ui/img/"))))
	http.Handle("/", rtr)
	http.ListenAndServe(":8080", nil)
}




func main() {

    handleFunc()



}
